package com.app.sample.shop.data.api;

import com.app.sample.shop.data.api.responses.BidResponse;
import com.app.sample.shop.data.api.responses.UserResponse;
import com.app.sample.shop.data.api.responses.WishResponse;
import com.app.sample.shop.models.Bid;
import com.app.sample.shop.models.Wish;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface AppRestEndpoints {

    @FormUrlEncoded
    @POST("user/register/")
    Call<UserResponse> signupUser(@Field("name") String name, @Field("phone") String phone, @Field("document") String document, @Field("email") String email,
                                  @Field("street") String street, @Field("city") String city, @Field("state") String state,
                                  @Field("username") String username, @Field("password") String password);

    @FormUrlEncoded
    @POST("user/login/")
    Call<UserResponse> loginUser(@Field("username") String username, @Field("password") String password);

    @GET("user/info/")
    Call<UserResponse> getUserProfile(@Query("user_id") String userId);

    @FormUrlEncoded
    @POST("user/rating/")
    Call<UserResponse> ratingUser(@Field("user_id") String userId, @Field("rating_value") double ratingValue);

    @FormUrlEncoded
    @POST("wish/register/")
    Call<WishResponse> createWish(@Field("name") String name, @Field("description") String description,
                                  @Field("image_url") String imageUrl, @Field("category") String category,
                                  @Field("user_id") String userId);

    @GET("/wish/all/")
    Call<List<WishResponse>> getAllWishes();

    @GET("/wish/category/")
    Call<List<WishResponse>> getWishesByCategory(@Query("category") String category);

    @GET("wish/by_id/")
    Call<WishResponse> getWishById(@Query("wish_id") String wishId);

    @FormUrlEncoded
    @POST("wish/delete/")
    Call<Void> deleteWish(@Field("wish_id") String wishId);

    @FormUrlEncoded
    @POST("bid/register/")
    Call<BidResponse> createBid(@Field("user_id") String userId, @Field("wish_id") String wishId,
                               @Field("value") double username);

    @GET("/wish/by_user_id/")
    Call<List<WishResponse>> getUserWishList(@Query("user_id") String userId);

    @GET("/bid/by_user_id/")
    Call<List<BidResponse>> getUserBidList(@Query("user_id") String userId);

    @GET("bid/by_wish_id/")
    Call<List<BidResponse>> getWishBidList(@Query("wish_id") String wishId);

    @FormUrlEncoded
    @POST("bid/accept/")
    Call<BidResponse> acceptBid(@Field("bid_id") String bidId);

    @FormUrlEncoded
    @POST("bid/cancel/")
    Call<BidResponse> cancelBid(@Field("bid_id") String bidId);

    @GET("bid/winner/")
    Call<BidResponse> getWishBidWinner(@Query("wish_id") String wishId);

}
