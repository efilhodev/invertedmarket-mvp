package com.app.sample.shop.interfaces;

import com.app.sample.shop.models.Wish;

public interface WishCreateMVP {
    interface Presenter{
        void requestCreateWish(String name, String description, String imageUrl, String userId,
                               String category);
    }
    interface View{
        void requestCreateWishSuccess(Wish wish);
        void requestCreateWishFailed(String message);
    }
}
