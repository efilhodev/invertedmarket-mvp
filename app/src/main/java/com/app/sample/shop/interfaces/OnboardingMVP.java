package com.app.sample.shop.interfaces;

import com.app.sample.shop.models.User;

public interface OnboardingMVP {
    interface Presenter{
        void requestUserSignup(String name, String phone, String document, String email, String street, String city,
                               String state,String username, String password);
    }
    interface View{
        void requestUserSignupSuccess(User user);
        void requestUserSignupFailed(String message);
    }
}
