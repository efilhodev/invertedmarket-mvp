package com.app.sample.shop.utils;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class DateUtils {

    public static String getDateFromMillis(long d) {
        SimpleDateFormat df = new SimpleDateFormat( "HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        return df.format(d);
    }
}
