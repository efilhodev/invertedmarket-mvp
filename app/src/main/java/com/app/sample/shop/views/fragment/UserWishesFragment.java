package com.app.sample.shop.views.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.sample.shop.R;
import com.app.sample.shop.data.Constant;
import com.app.sample.shop.interfaces.UserWishListMVP;
import com.app.sample.shop.models.User;
import com.app.sample.shop.models.Wish;
import com.app.sample.shop.presenters.UserWishListPresenter;
import com.app.sample.shop.views.activities.WishCreateActivity;
import com.app.sample.shop.views.activities.WishDetailActivity;
import com.app.sample.shop.views.adapter.UserWishesAdapter;
import com.app.sample.shop.views.widget.DividerItemDecoration;

import java.util.List;

public class UserWishesFragment extends BaseFragment implements UserWishListMVP.View {
    private View view;
    private LinearLayout lyt_not_found;

    private UserWishListMVP.Presenter presenter;
    private User user;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_wishes, null);
        lyt_not_found = view.findViewById(R.id.lyt_notfound);

        init();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        getBaseActivity().showLoading();
        presenter.requestWishList(this.user.getId());
    }

    private void init(){
        this.user = (User) getArguments().getSerializable(Constant.USER_KEY);

        if(null == presenter) presenter = new UserWishListPresenter(this);

        setupViews();
    }

    private void setupWishList(List<Wish> wishes){
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        UserWishesAdapter mAdapter = new UserWishesAdapter(getActivity(), wishes);
        recyclerView.setAdapter(mAdapter);
        mAdapter.SetOnItemClickListener(new UserWishesAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Wish obj) {

                switch (view.getId()){
                    case R.id.btn_wish_delete:
                        getBaseActivity().showLoading();
                        presenter.requestWishDelete(obj.getId());
                        break;
                    default:
                        WishDetailActivity.navigate(getBaseActivity(), view, obj, user);
                }
            }
        });
    }

    private void setupViews(){
        FloatingActionButton btnCreateWish = view.findViewById(R.id.btn_create);
        btnCreateWish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WishCreateActivity.navigate(getContext(), user);
            }
        });
    }

    @Override
    public void requestWishListSuccess(List<Wish> wishes) {
        getBaseActivity().hideLoading();
        if ((wishes.isEmpty())) {
            lyt_not_found.setVisibility(View.VISIBLE);
        } else {
            lyt_not_found.setVisibility(View.GONE);
        }
        setupWishList(wishes);
    }

    @Override
    public void requestWishListFailed(String message) {
        getBaseActivity().hideLoading();
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestWishDeleteSuccess() {
        getBaseActivity().hideLoading();
        Toast.makeText(getContext(), "Desejo deletado com sucesso!", Toast.LENGTH_SHORT).show();

        presenter.requestWishList(user.getId());
    }

    @Override
    public void requestWishDeleteFailed(String message) {
        getBaseActivity().hideLoading();
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
