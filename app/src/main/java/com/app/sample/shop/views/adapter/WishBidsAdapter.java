package com.app.sample.shop.views.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.models.Bid;
import com.app.sample.shop.models.User;
import com.app.sample.shop.views.widget.StringUtil;
import com.balysv.materialripple.MaterialRippleLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class WishBidsAdapter extends RecyclerView.Adapter<WishBidsAdapter.ViewHolder> implements Filterable {

    private final int mBackground;
    private List<Bid> original_items;
    private List<Bid> filtered_items;
    private WishBidsAdapter.ItemFilter mFilter = new WishBidsAdapter.ItemFilter();
    private final TypedValue mTypedValue = new TypedValue();
    private Context ctx;
    private UserBidsAdapter.OnItemClickListener mOnItemClickListener;
    private User user;

    public interface OnItemClickListener {
        void onItemClick(View view, int position, Bid obj);
    }

    public void SetOnItemClickListener(final UserBidsAdapter.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView name;
        public TextView rating;
        public TextView description;
        public Button btnAccept;
        public MaterialRippleLayout lyt_parent;

        public ViewHolder(View v) {
            super(v);
            name = v.findViewById(R.id.user_name);
            rating = v.findViewById(R.id.user_rating);
            description = v.findViewById(R.id.description);
            lyt_parent = v.findViewById(R.id.lyt_parent);
            btnAccept = v.findViewById(R.id.btn_bid_accept);
        }
    }

    public Filter getFilter() {
        return mFilter;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public WishBidsAdapter(Context ctx, List<Bid> items, User user) {
        this.ctx = ctx;
        original_items = items;
        filtered_items = items;
        ctx.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
        this.user = user;
    }

    @Override
    public WishBidsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wish_bid, parent, false);
        v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        WishBidsAdapter.ViewHolder vh = new WishBidsAdapter.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(WishBidsAdapter.ViewHolder holder, final int position) {
        final Bid bid = filtered_items.get(position);
        holder.name.setText(bid.getUser().getName());
        holder.rating.setText(String.valueOf(bid.getUser().getRating().getValue()) + "/5.0");
        holder.description.setText("Lance no valor de " +  StringUtil.convertCurrency(bid.getValue()));
        // view detail message conversation


        holder.btnAccept.setVisibility((user.getId().equals(bid.getWish().getUser().getId()) ? View.VISIBLE : View.GONE));

        holder.btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(view, position, bid);
                }
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String query = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();
            final List<Bid> list = original_items;
            final List<Bid> result_list = new ArrayList<>(list.size());

            for (int i = 0; i < list.size(); i++) {
                String str_title = list.get(i).getWish().getName();
                String str_cat = String.valueOf(list.get(i).getValue());
                if (str_title.toLowerCase().contains(query) || str_cat.toLowerCase().contains(query)) {
                    result_list.add(list.get(i));
                }
            }

            results.values = result_list;
            results.count = result_list.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filtered_items = (List<Bid>) results.values;
            notifyDataSetChanged();
        }

    }


}
