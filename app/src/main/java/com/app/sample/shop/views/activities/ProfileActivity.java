package com.app.sample.shop.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.sample.shop.R;
import com.app.sample.shop.interfaces.ProfileMVP;
import com.app.sample.shop.models.User;
import com.app.sample.shop.presenters.ProfilePresenter;

import butterknife.BindView;

public class ProfileActivity extends BaseActivity implements ProfileMVP.View {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.name)
    TextView tvName;
    @BindView(R.id.email)
    TextView tvEmail;
    @BindView(R.id.phone)
    TextView tvPhone;
    @BindView(R.id.street)
    TextView tvStreet;
    @BindView(R.id.city)
    TextView tvCity;
    @BindView(R.id.state)
    TextView tvState;
    @BindView(R.id.bid_count)
    TextView tvBidCount;
    @BindView(R.id.wish_count)
    TextView tvWishCount;
    @BindView(R.id.credit)
    TextView tvCredit;

    private User user;
    private ProfileMVP.Presenter presenter;

    @Override
    public int getLayout() {
        return R.layout.activity_profile;
    }

    public static void navigate (Context context, User user){
        Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtra("USER", user);

        context.startActivity(intent);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = (User) getIntent().getSerializableExtra("USER");
        if(null == presenter) presenter = new ProfilePresenter(this);


        showLoading();
        presenter.requestUserProfile(user.getId());


        initToolbar();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Perfil");
    }

    private void setupView(){
        tvName.setText(user.getName());
        tvEmail.setText(user.getEmail());
        tvPhone.setText(user.getPhone());
        tvStreet.setText(user.getStreet());
        tvCity.setText(user.getCity());
        tvState.setText(user.getState());
        tvBidCount.setText(String.valueOf(user.getBidCount()));
        tvWishCount.setText(String.valueOf(user.getWishCount()));
        tvCredit.setText(String.valueOf(user.getCredit()));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void requestUserProfileSuccess(User user) {
        hideLoading();
        this.user = user;

        setupView();
    }

    @Override
    public void requestUserProfileFailed(String message) {
        hideLoading();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

        setupView();
    }

    @Override
    public void requestUserRatingSuccess(User user) {
        hideLoading();
    }

    @Override
    public void requestUserRatingFailed(String message) {
        hideLoading();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
