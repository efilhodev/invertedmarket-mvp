package com.app.sample.shop.presenters;

import com.app.sample.shop.data.api.AppRestManager;
import com.app.sample.shop.data.api.responses.WishResponse;
import com.app.sample.shop.interfaces.WishCatalogMVP;
import com.app.sample.shop.models.Wish;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishCatalogPresenter implements WishCatalogMVP.Presenter {
    private WishCatalogMVP.View view;

    public WishCatalogPresenter(WishCatalogMVP.View view) {
        this.view = view;
    }

    @Override
    public void requestAllWishes() {
        Call<List<WishResponse>> request = AppRestManager.getInstance().getAppRestEndpoint().getAllWishes();

        request.enqueue(new Callback<List<WishResponse>>() {
            @Override
            public void onResponse(Call<List<WishResponse>> call, Response<List<WishResponse>> response) {
                switch (response.code()){
                    case 200:
                        List<Wish> wishes = new ArrayList<>();
                        if(response.body() != null){
                            for (WishResponse wishResponse : response.body()) {
                                wishes.add(wishResponse.getWish());
                            }
                        }
                        view.requestAllWishesSuccess(wishes);
                        break;
                    default:
                        view.requestAllWishesFailed("Erro ao buscar a lista com todos os desejos");
                }
            }

            @Override
            public void onFailure(Call<List<WishResponse>> call, Throwable t) {
                view.requestAllWishesFailed(t.getMessage());
            }
        });
    }

    @Override
    public void requestWishesByCategory(String category) {
        Call<List<WishResponse>> request = AppRestManager.getInstance().getAppRestEndpoint().getWishesByCategory(category);

        request.enqueue(new Callback<List<WishResponse>>() {
            @Override
            public void onResponse(Call<List<WishResponse>> call, Response<List<WishResponse>> response) {
                switch (response.code()){
                    case 200:
                        List<Wish> wishes = new ArrayList<>();
                        if(response.body() != null){
                            for (WishResponse wishResponse : response.body()) {
                                wishes.add(wishResponse.getWish());
                            }
                        }
                        view.requestWishesByCategorySuccess(wishes);
                        break;
                     default:
                         view.requestWishesByCategoryFailed("Erro ao buscar lista de desejos por categoria");
                }
            }

            @Override
            public void onFailure(Call<List<WishResponse>> call, Throwable t) {
                view.requestWishesByCategoryFailed(t.getMessage());
            }
        });
    }
}
