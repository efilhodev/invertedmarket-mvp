package com.app.sample.shop.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.app.sample.shop.R;
import com.app.sample.shop.interfaces.WishCreateMVP;
import com.app.sample.shop.models.User;
import com.app.sample.shop.models.Wish;
import com.app.sample.shop.presenters.WishCreatePresenter;
import com.app.sample.shop.views.adapter.SpinnerAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

public class WishCreateActivity extends BaseActivity implements WishCreateMVP.View {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.sp_wish_category)
    Spinner spWishCategory;
    @BindView(R.id.et_wish_url_image)
    EditText etWishUrlImage;
    @BindView(R.id.et_wish_name)
    EditText etWishName;
    @BindView(R.id.et_wish_description)
    EditText etWishDescription;
    @BindView(R.id.iv_wish_create)
    ImageView ivWishCreate;
    @BindView(R.id.til_wish_url_image)
    TextInputLayout tilWishUrlImage;
    @BindView(R.id.til_wish_name)
    TextInputLayout tilWishName;
    @BindView(R.id.til_wish_description)
    TextInputLayout tilWishDescription;

    private SpinnerAdapter adapter;
    private WishCreateMVP.Presenter presenter;
    private User user;



    @Override
    public int getLayout() {
        return R.layout.activity_wish_create;
    }

    public static void navigate (Context context, User user){
        Intent intent = new Intent(context, WishCreateActivity.class);
        intent.putExtra("USER", user);

        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(null == presenter) presenter = new WishCreatePresenter(this);
        this.user = (User) getIntent().getExtras().getSerializable("USER");

        initToolbar();
        initCategorySpinner();
        setupViews();
    }

    @OnClick(R.id.btn_create)
    public void onCreateWishClick(){

        if(validate()){
            showLoading();
            presenter.requestCreateWish(etWishName.getText().toString(), etWishDescription.getText().toString(),
                    etWishUrlImage.getText().toString(), user.getId(), adapter.getItem(spWishCategory.getSelectedItemPosition()));
        }
    }

    private void setupViews(){
        etWishUrlImage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Picasso.get().load(s.toString())
                        .resize(500,500)
                        .into(ivWishCreate);
            }
        });
    }

    private boolean validate(){
        if(etWishUrlImage.getText().toString().isEmpty()){
            tilWishUrlImage.setErrorEnabled(true);
            tilWishUrlImage.setError("Por favor adicione uma url da imagem para referencia");
            return false;
        }
        tilWishUrlImage.setErrorEnabled(false);

        if(etWishName.getText().toString().isEmpty()){
            tilWishName.setErrorEnabled(true);
            tilWishName.setError("O nome do desejo não pode ser vazio");

            return false;
        }
        tilWishName.setErrorEnabled(false);

        if(spWishCategory.getSelectedItemPosition() == 0){
            Toast.makeText(this, "Selecione uma categoria valida", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(etWishDescription.getText().toString().isEmpty()){
            tilWishDescription.setErrorEnabled(true);
            tilWishDescription.setError("O desejo deve possuir uma descrição");

            return false;
        }
        tilWishDescription.setErrorEnabled(false);


        return true;
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Criando um desejo");
    }

    private void initCategorySpinner(){
        adapter = new SpinnerAdapter(this,
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.wish_categories));
        spWishCategory.setAdapter(adapter);
    }

    @Override
    public void requestCreateWishSuccess(Wish wish) {
        hideLoading();
        Toast.makeText(this, "Seu desejo " + wish.getName() + "foi criado com sucesso!", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void requestCreateWishFailed(String message) {
        hideLoading();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
