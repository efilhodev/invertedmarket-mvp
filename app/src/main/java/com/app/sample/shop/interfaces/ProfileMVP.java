package com.app.sample.shop.interfaces;

import com.app.sample.shop.models.User;

public interface ProfileMVP {
    interface Presenter{
        void requestUserProfile(String userId);
        void requestUserRating(String userId, double ratingValue);
    }
    interface View{
        void requestUserProfileSuccess(User user);
        void requestUserProfileFailed(String message);

        void requestUserRatingSuccess(User user);
        void requestUserRatingFailed(String message);
    }
}
