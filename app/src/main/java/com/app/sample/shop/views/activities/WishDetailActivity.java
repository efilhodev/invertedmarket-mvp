package com.app.sample.shop.views.activities;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.sample.shop.R;
import com.app.sample.shop.data.Constant;
import com.app.sample.shop.interfaces.WishDetailsMVP;
import com.app.sample.shop.models.Bid;
import com.app.sample.shop.models.User;
import com.app.sample.shop.models.Wish;
import com.app.sample.shop.data.Tools;
import com.app.sample.shop.presenters.WishDetailPresenter;
import com.app.sample.shop.views.adapter.UserBidsAdapter;
import com.app.sample.shop.views.adapter.WishBidsAdapter;
import com.app.sample.shop.views.dialog.BidCreateDialog;
import com.app.sample.shop.views.widget.DividerItemDecoration;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class WishDetailActivity extends BaseActivity implements WishDetailsMVP.View {
    @BindView(R.id.tv_wish_name)
    TextView tvWishName;
    @BindView(R.id.tv_wish_description)
    TextView tvWishDescription;
    @BindView(R.id.tv_wish_owner_name)
    TextView tvWishOwnerName;
    @BindView(R.id.iv_wish_image)
    ImageView ivWishImage;
    @BindView(R.id.btn_bid)
    Button btnBid;
    @BindView(R.id.rv_wish_bids)
    RecyclerView rvWishBids;
    @BindView(R.id.cv_wish_bids)
    CardView cvWishBid;
    @BindView(R.id.tv_wish_owner_rating)
    TextView tvWishOwnerRating;
    @BindView(R.id.hidden_form)
    LinearLayout llOwnerUserInfo;
    @BindView(R.id.tv_wish_info_warning)
    TextView tvWishInfoWarning;


    private Wish wish;
    private User user;
    private WishDetailsMVP.Presenter presenter;
    private BidCreateDialog dialog;

    public static final String EXTRA_OBJCT = "com.app.sample.shop.ITEM";

    // give preparation animation activity transition
    public static void navigate(AppCompatActivity activity, View transitionImage, Wish obj, User user) {
        Intent intent = new Intent(activity, WishDetailActivity.class);
        intent.putExtra(EXTRA_OBJCT, obj);
        intent.putExtra(Constant.USER_KEY, user);

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, EXTRA_OBJCT);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    @Override
    public int getLayout() {
        return R.layout.activity_wish_details;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // animation transition
        ViewCompat.setTransitionName(findViewById(R.id.iv_wish_image), EXTRA_OBJCT);

        // get extra object
        wish = (Wish) getIntent().getSerializableExtra(EXTRA_OBJCT);
        user = (User) getIntent().getSerializableExtra(Constant.USER_KEY);

        initToolbar();
        // for system bar in lollipop
        Tools.systemBarLolipop(this);

        initRequest();
    }

    @OnClick(R.id.btn_bid)
    public void onCreateBidClick(){
        dialog = new BidCreateDialog(this, new BidCreateDialog.ValueCallback() {
            @Override
            public void getValue(double value) {
                dialog.dismiss();
                showLoading();
                presenter.requestBidForWish(user.getId(), wish.getId(), value);
            }
        });
        dialog.show();
    }

    public void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Detalhes do desejo");
    }

    private void setupViews(Wish wish){
        btnBid.setVisibility((user.getId().equals(wish.getUser().getId()) || wish.getStatus().equals("Finalizado") ? View.GONE : View.VISIBLE));
        cvWishBid.setVisibility(wish.getStatus().equals("Finalizado") ? View.GONE : View.VISIBLE);

        if(wish.getUser().getId().equals(user.getId())){
            tvWishInfoWarning.setVisibility(View.GONE);
            llOwnerUserInfo.setVisibility(View.VISIBLE);
        }

        tvWishName.setText(wish.getName());
        tvWishDescription.setText(wish.getDescription());
        tvWishOwnerName.setText(wish.getUser().getName());
        tvWishOwnerRating.setText(tvWishOwnerRating.getText().toString().replace("0.0", String.valueOf(wish.getUser().getRating().getValue())));

        Picasso.get().load(wish.getPhotoUrl())
                .resize(500, 500)
                .into(ivWishImage);

        rvWishBids.setLayoutManager(new LinearLayoutManager(this));
        rvWishBids.setHasFixedSize(true);
        rvWishBids.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
    }

    private void setupBidList(List<Bid> bids){

        WishBidsAdapter mAdapter = new WishBidsAdapter(this, bids, user);
        rvWishBids.setAdapter(mAdapter);
        mAdapter.SetOnItemClickListener(new UserBidsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Bid obj) {

                showLoading();
                presenter.requestAcceptBid(obj.getId());
            }
        });
    }

    private void initRequest(){
        if(null == presenter) presenter = new WishDetailPresenter(this);
        showLoading();
        presenter.requestWish(wish.getId());
        presenter.requestBidListByWish(wish.getId());
    }

    @Override
    public void requestWishSuccess(Wish wish) {
        hideLoading();
        setupViews(wish);

        showLoading();
        presenter.requestBidWishWinner(wish.getId());
    }

    @Override
    public void requestWishFailed(String message) {
        hideLoading();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestBidForWishSuccess(Bid bid) {
        hideLoading();
        presenter.requestBidListByWish(bid.getWish().getId());
        Toast.makeText(this, "Lance realizado com sucesso!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestBidForWishFailed(String message) {
        hideLoading();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestBidListByWishSuccess(List<Bid> bids) {
        hideLoading();
        setupBidList(bids);
    }

    @Override
    public void requestBidListByWishFailed(String message) {
        hideLoading();
        Toast.makeText(this, "Lance realizado com sucesso!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestAcceptBidSuccess(Bid bid) {
        hideLoading();
        presenter.requestBidListByWish(bid.getWish().getId());
        Toast.makeText(this, "Lance aceito com sucesso!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestAcceptBidFailed(String message) {
        hideLoading();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestBidWishWinnerSuccess(Bid bid) {
        hideLoading();
        if(bid.getUser().getId().equals(user.getId())){
            tvWishInfoWarning.setVisibility(View.GONE);
            llOwnerUserInfo.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void requestBidWishWinnerFailed(String message) {
        hideLoading();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
