package com.app.sample.shop.presenters;

import com.app.sample.shop.data.api.AppRestManager;
import com.app.sample.shop.data.api.responses.UserResponse;
import com.app.sample.shop.interfaces.ProfileMVP;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilePresenter implements ProfileMVP.Presenter {
    private ProfileMVP.View view;

    public ProfilePresenter(ProfileMVP.View view) {
        this.view = view;
    }


    @Override
    public void requestUserProfile(String userId) {
        Call<UserResponse> request = AppRestManager.getInstance().getAppRestEndpoint().getUserProfile(userId);

        request.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                switch (response.code()){
                    case 200:
                        view.requestUserProfileSuccess(response.body().getUser());
                        break;
                     default:
                         view.requestUserProfileFailed("Erro ao buscar dados do perfil.");
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                view.requestUserProfileFailed(t.getMessage());
            }
        });
    }

    @Override
    public void requestUserRating(String userId, double ratingValue) {
        Call<UserResponse> request = AppRestManager.getInstance().getAppRestEndpoint().ratingUser(userId,
                ratingValue);

        request.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                switch (response.code()){
                    case 200:
                        view.requestUserRatingSuccess(response.body().getUser());
                        break;
                    default:
                        view.requestUserRatingFailed("Erro ao avaliar este usuário");
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                view.requestUserRatingFailed(t.getMessage());
            }
        });
    }
}
