package com.app.sample.shop.utils;

import android.util.Log;

/**
 * Created by Eduardo Julio on 10/17/2017.
 */

public class Format {

    public static String firstAndLastName(String name) {
        if (name != null && !name.contains("[a-zA-Z]")) {
            String[] nameArray = name.split(" ");
            if (nameArray.length > 2) {
                return nameArray[0] + " " + nameArray[nameArray.length - 1];
            }
        }
        return name;
    }


    public static String convertCurrencyToDoubleString(String currency) {
        try {
            if (currency.equals("")) {
                currency = null;
            }
            return currency.replace("R$", "").replace(".", "").replace(",", ".").trim();
        } catch (NullPointerException e) {
            Log.e(Format.class.getSimpleName(), "NullPointerException occurred: Currency equals null or empty");
            return null;
        }
    }
}
