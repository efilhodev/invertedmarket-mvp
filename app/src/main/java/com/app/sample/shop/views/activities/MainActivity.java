package com.app.sample.shop.views.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.app.sample.shop.R;
import com.app.sample.shop.data.Constant;
import com.app.sample.shop.data.Tools;
import com.app.sample.shop.models.User;
import com.app.sample.shop.views.fragment.UserBidsFragment;
import com.app.sample.shop.views.fragment.UserWishesFragment;
import com.app.sample.shop.views.fragment.WishesCatalogFragment;

public class MainActivity extends BaseActivity {
    private DrawerLayout drawerLayout;
    private Toolbar mToolbar;
    private ActionBarDrawerToggle mDrawerToggle;
    private ActionBar actionBar;
    private Menu menu;
    private View parent_view;
    private NavigationView nav_view;
    private User user;

    public static void navigate (Context context, User user){
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("USER", user);

        context.startActivity(intent);

    }

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parent_view = findViewById(R.id.main_content);
        user = (User) getIntent().getSerializableExtra("USER");

        initToolbar();

        setupDrawerLayout();

        // display first page
        displayView(R.id.nav_new, getString(R.string.menu_new));
        actionBar.setTitle(R.string.menu_new);

        // for system bar in lollipop
        Tools.systemBarLolipop(this);
    }



    private void initToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void setupDrawerLayout() {
        drawerLayout = findViewById(R.id.drawer_layout);
        nav_view = findViewById(R.id.navigation_view);

        ((TextView)nav_view.getHeaderView(0).findViewById(R.id.header_title)).setText(user.getUsername());
        ((TextView)nav_view.getHeaderView(0).findViewById(R.id.header_title_name)).setText(user.getName());

        nav_view.getHeaderView(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileActivity.navigate(MainActivity.this, user);
            }
        });


        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        // Set the drawer toggle as the DrawerListener
        drawerLayout.setDrawerListener(mDrawerToggle);
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                drawerLayout.closeDrawers();
                actionBar.setTitle(menuItem.getTitle());
                displayView(menuItem.getItemId(), menuItem.getTitle().toString());
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.action_credit:
                Snackbar.make(parent_view, "Credit Clicked", Snackbar.LENGTH_SHORT).show();
                break;
            case R.id.action_settings:
                Snackbar.make(parent_view, "Setting Clicked", Snackbar.LENGTH_SHORT).show();
                break;
            case R.id.action_about: {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("About");
                builder.setMessage(getString(R.string.about_text));
                builder.setNeutralButton("OK", null);
                builder.show();
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void displayView(int id, String title) {
        actionBar.setDisplayShowCustomEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        Fragment fragment = null;
        Bundle bundle = new Bundle();
        switch (id) {
            case R.id.nav_bids:
                fragment = new UserBidsFragment();
                bundle.putSerializable(Constant.USER_KEY, user);
                break;
            case R.id.nav_wishes:
                fragment = new UserWishesFragment();
                bundle.putSerializable(Constant.USER_KEY, user);
                break;
            case R.id.nav_new:
                fragment = new WishesCatalogFragment();
                bundle.putSerializable(Constant.USER_KEY, user);
                break;

            //sub menu
            case R.id.nav_all:
                fragment = new WishesCatalogFragment();
                bundle.putSerializable(Constant.USER_KEY, user);
                break;
            case R.id.nav_clothing:
                fragment = new WishesCatalogFragment();
                bundle.putString(WishesCatalogFragment.TAG_CATEGORY, Constant.CLOATH_CATEGORY);
                bundle.putSerializable(Constant.USER_KEY, user);
                break;
            case R.id.nav_electronic:
                fragment = new WishesCatalogFragment();
                bundle.putString(WishesCatalogFragment.TAG_CATEGORY, Constant.ELECTRONIC_CATEGORY);
                bundle.putSerializable(Constant.USER_KEY, user);
                break;
            case R.id.nav_accessories:
                fragment = new WishesCatalogFragment();
                bundle.putString(WishesCatalogFragment.TAG_CATEGORY, Constant.ASSESSORY_CATEGORY);
                bundle.putSerializable(Constant.USER_KEY, user);
                break;
            default:
                break;
        }

        fragment.setArguments(bundle);

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_content, fragment);
            fragmentTransaction.commit();
            //initToolbar();
        }
    }

    private long exitTime = 0;

    public void doExitApp() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            Toast.makeText(this, R.string.press_again_exit_app, Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        doExitApp();
    }

    private void updateChartCounter(NavigationView nav, @IdRes int itemId, int count) {
        TextView view = (TextView) nav.getMenu().findItem(itemId).getActionView().findViewById(R.id.counter);
        view.setText(String.valueOf(count));
    }

}


