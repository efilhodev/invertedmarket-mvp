package com.app.sample.shop.views.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.sample.shop.data.Constant;
import com.app.sample.shop.interfaces.WishCatalogMVP;
import com.app.sample.shop.models.User;
import com.app.sample.shop.models.Wish;
import com.app.sample.shop.presenters.WishCatalogPresenter;
import com.app.sample.shop.views.activities.MainActivity;
import com.app.sample.shop.R;
import com.app.sample.shop.views.activities.WishDetailActivity;
import com.app.sample.shop.views.adapter.WishGridAdapter;
import com.app.sample.shop.data.Tools;

import java.util.List;

public class WishesCatalogFragment extends BaseFragment implements WishCatalogMVP.View {

    public static String TAG_CATEGORY = "com.app.sample.shop.tagCategory";

    private View view;
    private RecyclerView recyclerView;
    private WishGridAdapter mAdapter;
    private LinearLayout lytNotFound;
    private String category = null;
    private WishCatalogMVP.Presenter presenter;
    private User user;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_category, null);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
        setupWishList();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(TextUtils.isEmpty(category)){
            requestAllWishes();
        }else {
            requestCategoryWishes();
        }

    }

    private void init(){
        category = getArguments().getString(TAG_CATEGORY);
        recyclerView =  view.findViewById(R.id.recyclerView);
        lytNotFound =  view.findViewById(R.id.lyt_notfound);

        this.user = (User) getArguments().getSerializable(Constant.USER_KEY);
    }

    private void requestAllWishes(){
        if(null == presenter) presenter = new WishCatalogPresenter(this);
        presenter.requestAllWishes();
        getBaseActivity().showLoading();
    }

    private void requestCategoryWishes(){
        if(null == presenter) presenter = new WishCatalogPresenter(this);
        presenter.requestWishesByCategory(category);
        getBaseActivity().showLoading();
    }

    private void setupWishList(){
        LinearLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), Tools.getGridSpanCount(getActivity()));
        recyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new WishGridAdapter(getActivity());
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new WishGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Wish obj, int position) {
                WishDetailActivity.navigate((MainActivity)getActivity(), v.findViewById(R.id.lyt_parent), obj, user);
            }
        });
    }

    @Override
    public void requestAllWishesSuccess(List<Wish> wishes) {
        getBaseActivity().hideLoading();

        if(wishes.isEmpty()){
            lytNotFound.setVisibility(View.VISIBLE);
        }else{
            lytNotFound.setVisibility(View.GONE);
            mAdapter.setDisableAnimation(true);
            mAdapter.setData(wishes);
        }
    }

    @Override
    public void requestAllWishesFailed(String message) {
        getBaseActivity().hideLoading();
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void requestWishesByCategorySuccess(List<Wish> wishes) {
        getBaseActivity().hideLoading();

        if(wishes.isEmpty()){
            lytNotFound.setVisibility(View.VISIBLE);
        }else{
            lytNotFound.setVisibility(View.GONE);
            mAdapter.setDisableAnimation(true);
            mAdapter.setData(wishes);
        }
    }

    @Override
    public void requestWishesByCategoryFailed(String message) {
        getBaseActivity().hideLoading();
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }
}
