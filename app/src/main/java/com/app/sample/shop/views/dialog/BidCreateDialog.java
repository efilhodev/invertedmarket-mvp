package com.app.sample.shop.views.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.sample.shop.R;
import com.app.sample.shop.utils.Format;
import com.app.sample.shop.views.widget.MoneyWatcher;

public class BidCreateDialog extends Dialog {
   public interface ValueCallback {
        void getValue(double value);
    }

    private ValueCallback callback;

    public BidCreateDialog(Context context, ValueCallback callback) {
        super(context);
        this.callback = callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_create_bid);

        setupViews();
    }


    private void setupViews(){
        final EditText etBidValue = findViewById(R.id.et_bid_value);
        etBidValue.addTextChangedListener(new MoneyWatcher(etBidValue));
        Button btnEnter = findViewById(R.id.btn_enter);
        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.getValue(Double.parseDouble(Format.convertCurrencyToDoubleString(etBidValue.getText().toString())));
            }
        });
    }
}
