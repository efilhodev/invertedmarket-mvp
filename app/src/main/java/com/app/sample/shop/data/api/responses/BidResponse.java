package com.app.sample.shop.data.api.responses;

import com.app.sample.shop.models.Bid;
import com.google.gson.annotations.SerializedName;

public class BidResponse {

    @SerializedName("Bid")
    private Bid bid;

    public BidResponse(Bid bid) {
        this.bid = bid;
    }

    public Bid getBid() {
        return bid;
    }
}
