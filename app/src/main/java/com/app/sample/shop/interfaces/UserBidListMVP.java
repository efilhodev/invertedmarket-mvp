package com.app.sample.shop.interfaces;

import com.app.sample.shop.models.Bid;

import java.util.List;

public interface UserBidListMVP {
    interface Presenter{
        void requestUserBidList(String userId);
        void requestCancelBid(String bidId);
    }
    interface View{
        void requestUserBidListSuccess(List<Bid> bids);
        void requestUserBidListFailed(String message);

        void requestCancelBidSuccess(Bid bid);
        void requestCancelBidFailed(String message);
    }
}
