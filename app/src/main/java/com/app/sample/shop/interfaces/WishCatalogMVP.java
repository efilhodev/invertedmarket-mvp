package com.app.sample.shop.interfaces;

import com.app.sample.shop.models.Wish;

import java.util.List;

public interface WishCatalogMVP {
    interface Presenter{
        void requestAllWishes();
        void requestWishesByCategory(String category);
    }
    interface View{
        void requestAllWishesSuccess(List<Wish> wishes);
        void requestAllWishesFailed(String message);

        void requestWishesByCategorySuccess(List<Wish> wishes);
        void requestWishesByCategoryFailed(String message);
    }
}
