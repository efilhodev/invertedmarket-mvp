package com.app.sample.shop.views.adapter;

import android.content.Context;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.util.TimeUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.sample.shop.R;
import com.app.sample.shop.models.Wish;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.app.sample.shop.utils.DateUtils.getDateFromMillis;

public class WishGridAdapter extends RecyclerView.Adapter<WishGridAdapter.ViewHolder> implements Filterable {

    private final int mBackground;
    private List<Wish> original_items;
    private List<Wish> filtered_items;
    private ItemFilter mFilter = new ItemFilter();

    private final TypedValue mTypedValue = new TypedValue();
    private boolean disableAnimation;

    private Context ctx;

    // for item click listener
    private OnItemClickListener mOnItemClickListener;
    public interface OnItemClickListener {
        void onItemClick(View view, Wish obj, int position);
    }
    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title;
        public ImageView image;
        public RelativeLayout lyt_parent;
        public TextView username;
        private TextView timer;
        private TextView bitCount;
        private CountDownTimer countDownTimer;

        public ViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.title);
            image = v.findViewById(R.id.image);
            lyt_parent = v.findViewById(R.id.lyt_parent);
            username = v.findViewById(R.id.user_name);
            timer = v.findViewById(R.id.timer);
            bitCount = v.findViewById(R.id.bid_count);
        }


        void setupCountDownTimer(long time){

            if(countDownTimer != null) return;

            countDownTimer = new CountDownTimer(time, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    timer.setText(getDateFromMillis(millisUntilFinished));
                }

                @Override
                public void onFinish() {

                }
            };
            countDownTimer.start();
        }

    }

    public Filter getFilter() {
        return mFilter;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public WishGridAdapter(Context ctx) {
        this.ctx = ctx;
        original_items = new ArrayList<>();
        filtered_items = new ArrayList<>();
        ctx.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        mBackground = mTypedValue.resourceId;
    }

    @Override
    public WishGridAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wish_grid, parent, false);
        v.setBackgroundResource(mBackground);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Wish wish = filtered_items.get(position);
        holder.title.setText(wish.getName());
        Picasso.get()
                .load(wish.getPhotoUrl())
                .resize(500, 500)
                .into(holder.image);

        // Here you apply the animation when the view is bound
        setAnimation(holder.itemView, position);

        holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mOnItemClickListener != null){
                    mOnItemClickListener.onItemClick(view, wish, position);
                }
            }
        });

        holder.username.setText(wish.getUser().getUsername());

        holder.bitCount.setText(String.valueOf(wish.getBidCount()));

        holder.setupCountDownTimer(wish.getDeadline().getTime() - new Date().getTime());
    }

    public void setData(List<Wish> wishes){
        original_items.clear();
        filtered_items.clear();

        original_items = wishes;
        filtered_items = wishes;
        notifyDataSetChanged();
    }

    /**
     * Here is the key method to apply the animation
     */
    private int lastPosition = -1;
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (disableAnimation && position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(ctx, R.anim.slide_in_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    public void setDisableAnimation(boolean disable){
        disableAnimation = disable;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String query = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();
            final List<Wish> list = original_items;
            final List<Wish> result_list = new ArrayList<>(list.size());

            for (int i = 0; i < list.size(); i++) {
                String str_title = list.get(i).getName();
                String str_cat = list.get(i).getCategory();
                if (str_title.toLowerCase().contains(query) || str_cat.toLowerCase().contains(query)) {
                    result_list.add(list.get(i));
                }
            }

            results.values = result_list;
            results.count = result_list.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filtered_items = (List<Wish>) results.values;
            notifyDataSetChanged();
        }

    }
}