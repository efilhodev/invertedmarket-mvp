package com.app.sample.shop.views.widget;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.NumberFormat;
import java.util.Locale;


public class MoneyWatcher implements TextWatcher {
    private boolean isUpdating;
    private EditText editText;
    private int modifier = 100;

    public MoneyWatcher(EditText editText) {
        this.editText = editText;
    }

    @Override
    public void onTextChanged(CharSequence amount, int start, int before, int count) {
        if (isUpdating) {
            isUpdating = false;
            return;
        }

        isUpdating = true;

        String cleanString = clearCurrencyToNumber(amount.toString());

        if (cleanString.isEmpty()) {
            String formattedAmount = "R$" + transformToCurrency("0");

            editText.setText(formattedAmount);
            editText.setSelection(formattedAmount.length());
        }
        else {

            try {

                String formattedAmount = "R$" + transformToCurrency(cleanString);

                editText.setText(formattedAmount);
                editText.setSelection(formattedAmount.length());
            } catch (Exception e) {
                //log something
            }
        }
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private String transformToCurrency(String value) {
        double parsed = Double.parseDouble(value);
        String formatted = NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format((parsed / modifier));
        formatted = formatted.replaceAll("[^(0-9)(.,)]", "");
        return formatted;
    }

    private String clearCurrencyToNumber(String currencyValue) {
        String result;

        if (currencyValue == null) {
            result = "";
        } else {
            result = currencyValue.replaceAll("[(a-z)|(A-Z)|($,. )]", "");
        }
        return result;
    }
}