package com.app.sample.shop.interfaces;

import com.app.sample.shop.models.User;

public interface LoginMVP {
    interface Presenter{
        void requestLogin(String email, String password);
    }
    interface View{
        void requestLoginSuccess(User user);
        void requestLoginFailed(String message);
    }
}
