package com.app.sample.shop.presenters;

import com.app.sample.shop.data.api.AppRestManager;
import com.app.sample.shop.data.api.responses.UserResponse;
import com.app.sample.shop.interfaces.OnboardingMVP;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnboardingPresenter implements OnboardingMVP.Presenter {
    private OnboardingMVP.View view;

    public OnboardingPresenter(OnboardingMVP.View view) {
        this.view = view;
    }

    @Override
    public void requestUserSignup(String name, String phone, String document, String email, String street, String city,
                                  String state,String username, String password) {

        Call<UserResponse> call = AppRestManager.getInstance().getAppRestEndpoint()
                .signupUser(name, phone, document, email, street, city, state, username, password);

        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                switch (response.code()){
                    case 200:
                        view.requestUserSignupSuccess(response.body().getUser());
                        break;

                    default:
                        view.requestUserSignupFailed("Cadastro inválido.");
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                view.requestUserSignupFailed(t.getMessage());
            }
        });
    }
}
