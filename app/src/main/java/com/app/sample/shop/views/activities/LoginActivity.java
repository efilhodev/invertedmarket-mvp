package com.app.sample.shop.views.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.Toast;

import com.app.sample.shop.R;
import com.app.sample.shop.interfaces.LoginMVP;
import com.app.sample.shop.models.User;
import com.app.sample.shop.presenters.LoginPresenter;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginMVP.View {
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;

    private LoginMVP.Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(null == presenter) presenter = new LoginPresenter(this);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_login;
    }

    @OnClick(R.id.btn_enter)
    public void onEnterClick(){
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        if(validade()){
            showLoading();
            presenter.requestLogin(username, password);
        }
    }

    @OnClick(R.id.btn_signup)
    public void onSignupClick(){
        OnboardingActivity.navigate(this);
    }

    @Override
    public void requestLoginSuccess(User user) {
        hideLoading();
        MainActivity.navigate(this, user);
        clearFields();
    }

    @Override
    public void requestLoginFailed(String message) {
        hideLoading();
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void clearFields(){
        etUsername.setText("");
        etPassword.setText("");
    }

    private boolean validade() {
        if (etUsername.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "O campo nome de usuário não pode ser vazio.", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etPassword.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "O campo senha não pode ser vazio.", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
