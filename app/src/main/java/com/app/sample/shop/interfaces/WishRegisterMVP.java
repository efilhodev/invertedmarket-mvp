package com.app.sample.shop.interfaces;

import com.app.sample.shop.models.Wish;

public interface WishRegisterMVP {
    interface Presenter{
        void requestCreateWish(String name, String description, String imageUrl, String category, String user_id);
    }
    interface View{
        void requestCreateWishSuccess(Wish wish);
        void requestCreateWishFailed(String message);
    }
}
