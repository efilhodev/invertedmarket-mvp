package com.app.sample.shop.views.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

@SuppressLint("Registered")
public abstract class BaseActivity extends AppCompatActivity {
    private ProgressDialog dialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        ButterKnife.bind(this);
    }

    public abstract @LayoutRes int getLayout();

    public void showLoading(){
        if(null == dialog) dialog = ProgressDialog.show(this, "Aguarde.",
                "Processando..!", true);
        dialog.setCancelable(false);
        if(!dialog.isShowing()) dialog.show();
    }

    public void hideLoading(){
        if(null != dialog) dialog.dismiss();
    }
}
