package com.app.sample.shop.models;

import java.io.Serializable;

public class Rating implements Serializable {
    private double value;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
