package com.app.sample.shop.interfaces;

import com.app.sample.shop.models.Bid;
import com.app.sample.shop.models.Wish;

import java.util.List;

public interface WishDetailsMVP {
    interface Presenter{
        void requestWish(String wishId);
        void requestBidForWish(String userId, String wishId, double value);
        void requestBidListByWish(String wishId);
        void requestAcceptBid(String wishId);
        void requestBidWishWinner(String wishId);
    }
    interface View{
        void requestWishSuccess(Wish wish);
        void requestWishFailed(String message);

        void requestBidForWishSuccess(Bid bid);
        void requestBidForWishFailed(String message);

        void requestBidListByWishSuccess(List<Bid> bids);
        void requestBidListByWishFailed(String message);

        void requestAcceptBidSuccess(Bid bid);
        void requestAcceptBidFailed(String message);

        void requestBidWishWinnerSuccess(Bid bid);
        void requestBidWishWinnerFailed(String message);
    }
}
