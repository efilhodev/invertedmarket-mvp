package com.app.sample.shop.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Wish implements Serializable {
    private long id;
    private String name;
    private String category;
    private String description;
    @SerializedName("image_url")
    private String photoUrl;
    @SerializedName("User")
    private User user;
    private String status;
    @SerializedName("bid_count")
    private int bidCount;
    @SerializedName("deadline")
    private Date deadline;

    public String getId() {
        return String.valueOf(id);
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getStatus() {
        switch (status){
            case "FINISHED":
                return "Finalizado";
            default:
                return "Aberto";
        }
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getBidCount() {
        return bidCount;
    }

    public void setBidCount(int bidCount) {
        this.bidCount = bidCount;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }
}
