package com.app.sample.shop.data.api.responses;

import com.app.sample.shop.models.Wish;
import com.google.gson.annotations.SerializedName;

public class WishResponse {

    @SerializedName("Wish")
    private Wish wish;

    public WishResponse(Wish wish) {
        this.wish = wish;
    }

    public Wish getWish() {
        return wish;
    }
}
