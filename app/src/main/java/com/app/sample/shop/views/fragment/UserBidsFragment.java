package com.app.sample.shop.views.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.sample.shop.R;
import com.app.sample.shop.interfaces.UserBidListMVP;
import com.app.sample.shop.models.Bid;
import com.app.sample.shop.models.User;
import com.app.sample.shop.presenters.UserBidListPresenter;
import com.app.sample.shop.views.activities.WishDetailActivity;
import com.app.sample.shop.views.adapter.UserBidsAdapter;
import com.app.sample.shop.views.widget.DividerItemDecoration;

import java.util.List;

public class UserBidsFragment extends BaseFragment implements UserBidListMVP.View {
    private View view;
    private LinearLayout lyt_not_found;

    private UserBidListMVP.Presenter presenter;
    private User user;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bids, null);
        lyt_not_found = view.findViewById(R.id.lyt_notfound);

        init();
        return view;
    }

    private void init(){
        user = (User) getArguments().getSerializable("USER");

        if(null == presenter) presenter = new UserBidListPresenter(this);

        getBaseActivity().showLoading();
        presenter.requestUserBidList(user.getId());
    }

    private void setupBidList(List<Bid> bids){
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));

        UserBidsAdapter mAdapter = new UserBidsAdapter(getActivity(), bids);
        recyclerView.setAdapter(mAdapter);
        mAdapter.SetOnItemClickListener(new UserBidsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, Bid obj) {
                switch (view.getId()){
                    case R.id.btn_bid_cancel:

                        getBaseActivity().showLoading();
                        presenter.requestCancelBid(obj.getId());

                        break;
                    default:
                        WishDetailActivity.navigate(getBaseActivity(), view, obj.getWish(), user);
                }
            }
        });
    }

    @Override
    public void requestUserBidListSuccess(List<Bid> bids) {
        getBaseActivity().hideLoading();
        if ((bids.isEmpty())) {
            lyt_not_found.setVisibility(View.VISIBLE);
        } else {
            lyt_not_found.setVisibility(View.GONE);
            setupBidList(bids);
        }
    }

    @Override
    public void requestUserBidListFailed(String message) {
        getBaseActivity().hideLoading();
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requestCancelBidSuccess(Bid bid) {
        getBaseActivity().hideLoading();
        Toast.makeText(getContext(), "Lance cancelado com sucesso!", Toast.LENGTH_SHORT).show();

        getBaseActivity().showLoading();
        presenter.requestUserBidList(user.getId());
    }

    @Override
    public void requestCancelBidFailed(String message) {
        getBaseActivity().hideLoading();
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
