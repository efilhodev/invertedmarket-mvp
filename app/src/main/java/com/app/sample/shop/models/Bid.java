package com.app.sample.shop.models;

import com.google.gson.annotations.SerializedName;

public class Bid {
    private long id;
    @SerializedName("User")
    private User user;
    @SerializedName("Wish")
    private Wish wish;
    private double value;
    @SerializedName("status")
    private String status;

    public String getId() {
        return String.valueOf(id);
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Wish getWish() {
        return wish;
    }

    public void setWish(Wish wish) {
        this.wish = wish;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getStatus() {

        switch (status){
            case "ACCEPTED":
                return "Aceito";
            case "CLOSED":
                return "Finalizado";
            case "CANCELLED":
                return "Cancelado";

            default:
                return "Enviado";
        }
    }
}
