package com.app.sample.shop.interfaces;

import com.app.sample.shop.models.Wish;

import java.util.List;

public interface UserWishListMVP {
    interface Presenter{
        void requestWishList(String userId);
        void requestWishDelete(String wishId);
    }

    interface View{
        void requestWishListSuccess(List<Wish> wishes);
        void requestWishListFailed(String message);

        void requestWishDeleteSuccess();
        void requestWishDeleteFailed(String message);
    }
}
