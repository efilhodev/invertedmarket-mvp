package com.app.sample.shop.data;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.Log;

import com.app.sample.shop.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@SuppressWarnings("ResourceType")
public class Constant {
    public static String BASE_URL = "http://10.0.2.2:8100/";
    public static String USER_KEY = "USER";

    public static String CLOATH_CATEGORY = "Vestimenta";
    public static String ELECTRONIC_CATEGORY = "Eletrônico";
    public static String ASSESSORY_CATEGORY = "Acessórios";
}
