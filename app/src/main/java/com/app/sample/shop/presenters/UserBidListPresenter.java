package com.app.sample.shop.presenters;

import com.app.sample.shop.data.api.AppRestManager;
import com.app.sample.shop.data.api.responses.BidResponse;
import com.app.sample.shop.interfaces.UserBidListMVP;
import com.app.sample.shop.models.Bid;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserBidListPresenter implements UserBidListMVP.Presenter {
    private UserBidListMVP.View view;

    public UserBidListPresenter(UserBidListMVP.View view) {
        this.view = view;
    }

    @Override
    public void requestUserBidList(String userId) {
        Call<List<BidResponse>> request = AppRestManager.getInstance().getAppRestEndpoint().getUserBidList(userId);

        request.enqueue(new Callback<List<BidResponse>>() {
            @Override
            public void onResponse(Call<List<BidResponse>> call, Response<List<BidResponse>> response) {
                switch (response.code()){
                    case 200:
                        List<Bid> bids = new ArrayList<>();
                        if(response.body() != null){
                            for (BidResponse bidResponse : response.body()) {
                                bids.add(bidResponse.getBid());
                            }
                        }
                        view.requestUserBidListSuccess(bids);
                        break;
                    default:
                        view.requestUserBidListFailed("Erro ao buscar a lista de lances");
                }
            }

            @Override
            public void onFailure(Call<List<BidResponse>> call, Throwable t) {
                view.requestUserBidListFailed(t.getMessage());
            }
        });

    }

    @Override
    public void requestCancelBid(String bidId) {
        Call<BidResponse> request = AppRestManager.getInstance().getAppRestEndpoint().cancelBid(bidId);

        request.enqueue(new Callback<BidResponse>() {
            @Override
            public void onResponse(Call<BidResponse> call, Response<BidResponse> response) {
                switch (response.code()){
                    case 200:
                        view.requestCancelBidSuccess(response.body().getBid());
                        break;
                    default:
                        view.requestCancelBidFailed("Erro ao cancelar o lance");
                }
            }

            @Override
            public void onFailure(Call<BidResponse> call, Throwable t) {
                view.requestCancelBidFailed(t.getMessage());
            }
        });
    }
}
