package com.app.sample.shop.data.api;

import com.app.sample.shop.data.Constant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppRestManager {
    private static final AppRestManager ourInstance = new AppRestManager();
    private Retrofit retrofit;



    public static AppRestManager getInstance() {
        return ourInstance;
    }

    private AppRestManager(){
        if(null == retrofit){
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd HH:mm:ss")
                    .create();

            retrofit = new Retrofit.Builder().baseUrl(Constant.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson)).build();
        }
    }

    public AppRestEndpoints getAppRestEndpoint(){
        return retrofit.create(AppRestEndpoints.class);
    }
}
