package com.app.sample.shop.presenters;

import com.app.sample.shop.data.api.AppRestManager;
import com.app.sample.shop.data.api.responses.BidResponse;
import com.app.sample.shop.data.api.responses.WishResponse;
import com.app.sample.shop.interfaces.UserWishListMVP;
import com.app.sample.shop.models.Wish;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserWishListPresenter implements UserWishListMVP.Presenter {
    private UserWishListMVP.View view;

    public UserWishListPresenter(UserWishListMVP.View view) {
        this.view = view;
    }

    @Override
    public void requestWishList(String userId) {
        Call<List<WishResponse>> request = AppRestManager.getInstance().getAppRestEndpoint().getUserWishList(userId);

        request.enqueue(new Callback<List<WishResponse>>() {
            @Override
            public void onResponse(Call<List<WishResponse>> call, Response<List<WishResponse>> response) {
                switch (response.code()){
                    case 200:
                        List<Wish> wishes = new ArrayList<>();
                        if(response.body() != null){
                            for (WishResponse wishResponse : response.body()) {
                                wishes.add(wishResponse.getWish());
                            }
                        }
                        view.requestWishListSuccess(wishes);
                        break;
                    default:
                        view.requestWishListFailed("Erro ao buscar a lista de desejos deste usuário.");

                }
            }

            @Override
            public void onFailure(Call<List<WishResponse>> call, Throwable t) {
                view.requestWishListFailed(t.getMessage());
            }
        });
    }

    @Override
    public void requestWishDelete(String wishId) {
        Call<Void> request = AppRestManager.getInstance().getAppRestEndpoint().deleteWish(wishId);

        request.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                switch (response.code()){
                    case 200:
                        view.requestWishDeleteSuccess();
                        break;
                    default:
                        view.requestWishDeleteFailed("Erro ao deletar o desejo.");

                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                view.requestWishDeleteFailed(t.getMessage());
            }
        });
    }
}
