package com.app.sample.shop.views.widget;

import java.text.NumberFormat;
import java.util.Locale;

public class StringUtil {

    public static String convertCurrency(double value){
        Double d = value;
        Locale ptBr = new Locale("pt", "BR");

        return NumberFormat.getCurrencyInstance(ptBr).format(d);
    }
}
