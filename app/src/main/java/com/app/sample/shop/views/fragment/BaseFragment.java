package com.app.sample.shop.views.fragment;

import android.support.v4.app.Fragment;

import com.app.sample.shop.views.activities.BaseActivity;

public class BaseFragment extends Fragment {

    protected BaseActivity getBaseActivity(){
        return (BaseActivity) getActivity();
    }
}
