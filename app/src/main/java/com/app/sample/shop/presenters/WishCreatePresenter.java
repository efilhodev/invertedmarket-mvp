package com.app.sample.shop.presenters;

import com.app.sample.shop.data.api.AppRestManager;
import com.app.sample.shop.data.api.responses.WishResponse;
import com.app.sample.shop.interfaces.WishCreateMVP;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishCreatePresenter implements WishCreateMVP.Presenter {
    private WishCreateMVP.View view;


    public WishCreatePresenter(WishCreateMVP.View view) {
        this.view = view;
    }

    @Override
    public void requestCreateWish(String name, String description, String imageUrl, String userId, String category) {
        Call<WishResponse> request = AppRestManager.getInstance().getAppRestEndpoint().createWish(name,
                description, imageUrl, category, userId);

        request.enqueue(new Callback<WishResponse>() {
            @Override
            public void onResponse(Call<WishResponse> call, Response<WishResponse> response) {
                switch (response.code()){
                    case 200:
                        view.requestCreateWishSuccess(response.body().getWish());
                        break;
                    default:
                        view.requestCreateWishFailed("Erro ao criar um novo desejo");

                }
            }

            @Override
            public void onFailure(Call<WishResponse> call, Throwable t) {
                view.requestCreateWishFailed(t.getMessage());
            }
        });
    }
}
