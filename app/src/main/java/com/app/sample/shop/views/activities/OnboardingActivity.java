package com.app.sample.shop.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.Toast;

import com.app.sample.shop.R;
import com.app.sample.shop.interfaces.OnboardingMVP;
import com.app.sample.shop.models.User;
import com.app.sample.shop.presenters.OnboardingPresenter;
import com.app.sample.shop.views.widget.MaskWatcher;

import butterknife.BindView;
import butterknife.OnClick;

public class OnboardingActivity extends BaseActivity implements OnboardingMVP.View {
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_document)
    EditText etCpf;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_street)
    EditText etStreet;
    @BindView(R.id.et_city)
    EditText etCity;
    @BindView(R.id.et_state)
    EditText etState;
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_confirm_password)
    EditText etPasswordConfirm;

    private OnboardingMVP.Presenter presenter;

    public static void navigate(Context context){
        context.startActivity(new Intent(context, OnboardingActivity.class));
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(null == presenter) presenter = new OnboardingPresenter(this);

        etCpf.addTextChangedListener(MaskWatcher.buildCPF());
        etPhone.addTextChangedListener(MaskWatcher.buildPhone());
    }

    @Override
    public int getLayout() {
        return R.layout.activity_onboarding;
    }

    @Override
    public void requestUserSignupSuccess(User user) {
        MainActivity.navigate(this, user);
        finish();
    }

    @Override
    public void requestUserSignupFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_signup)
    public void onSignupClick(){

        String name = etName.getText().toString();
        String phone = etPhone.getText().toString();
        String cpf = etCpf.getText().toString();
        String email = etEmail.getText().toString();
        String street = etStreet.getText().toString();
        String city = etCity.getText().toString();
        String state = etState.getText().toString();
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        if(validade()){
            presenter.requestUserSignup(name, phone, cpf, email, street, city, state, username, password);
        }
    }

    private boolean validade(){
        if(etName.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "O campo nome não pode ser vazio.", Toast.LENGTH_LONG).show();
            return false;
        }
        if(etPhone.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "O campo telefone não pode ser vazio.", Toast.LENGTH_LONG).show();
            return false;
        }
        if(etCpf.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "O campo cpf não pode ser vazio.", Toast.LENGTH_LONG).show();
            return false;
        }
        if(etEmail.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "O campo email não pode ser vazio.", Toast.LENGTH_LONG).show();
            return false;
        }
        if(etStreet.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "O campo rua não pode ser vazio.", Toast.LENGTH_LONG).show();
            return false;
        }
        if(etCity.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "O campo cidade não pode ser vazio.", Toast.LENGTH_LONG).show();
            return false;
        }
        if(etState.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "O campo estado não pode ser vazio.", Toast.LENGTH_LONG).show();
            return false;
        }
        if(etPassword.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "O campo senha não pode ser vazio.", Toast.LENGTH_LONG).show();
            return false;
        }
        if(etUsername.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "O campo nome de usuário não pode ser vazio.", Toast.LENGTH_LONG).show();
            return false;
        }
        if(!etPassword.getText().toString().equals(etPasswordConfirm.getText().toString())){
            Toast.makeText(this, "Confirmação de senha inválida.", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }
}
