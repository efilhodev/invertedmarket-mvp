package com.app.sample.shop.presenters;


import com.app.sample.shop.data.api.AppRestManager;
import com.app.sample.shop.data.api.responses.BidResponse;
import com.app.sample.shop.data.api.responses.WishResponse;
import com.app.sample.shop.interfaces.WishDetailsMVP;
import com.app.sample.shop.models.Bid;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WishDetailPresenter implements WishDetailsMVP.Presenter {
    private WishDetailsMVP.View view;

    public WishDetailPresenter(WishDetailsMVP.View view) {
        this.view = view;
    }

    @Override
    public void requestWish(String wishId) {
        Call<WishResponse> request = AppRestManager.getInstance().getAppRestEndpoint().getWishById(wishId);

        request.enqueue(new Callback<WishResponse>() {
            @Override
            public void onResponse(Call<WishResponse> call, Response<WishResponse> response) {
                switch (response.code()){
                    case 200:
                        view.requestWishSuccess(response.body().getWish());
                        break;
                    default:
                        view.requestWishFailed("Erro ao buscar o desejo");

                }
            }

            @Override
            public void onFailure(Call<WishResponse> call, Throwable t) {
                view.requestWishFailed(t.getMessage());
            }
        });
    }

    @Override
    public void requestBidForWish(String userId, String wishId, double value) {
        Call<BidResponse> request = AppRestManager.getInstance().getAppRestEndpoint().createBid(userId,
                wishId, value);

        request.enqueue(new Callback<BidResponse>() {
            @Override
            public void onResponse(Call<BidResponse> call, Response<BidResponse> response) {
                switch (response.code()){
                    case 200:
                        view.requestBidForWishSuccess(response.body().getBid());
                        break;
                    default:
                        view.requestBidForWishFailed("Erro ao criar o lance");
                }
            }

            @Override
            public void onFailure(Call<BidResponse> call, Throwable t) {
                view.requestBidForWishFailed(t.getMessage());
            }
        });
    }

    @Override
    public void requestBidListByWish(String wishId) {
        Call<List<BidResponse>> request = AppRestManager.getInstance().getAppRestEndpoint().getWishBidList(wishId);

        request.enqueue(new Callback<List<BidResponse>>() {
            @Override
            public void onResponse(Call<List<BidResponse>> call, Response<List<BidResponse>> response) {
                switch (response.code()){
                    case 200:
                        List<Bid> bids = new ArrayList<>();
                        if(response.body() != null){
                            for (BidResponse bidResponse : response.body()) {
                                bids.add(bidResponse.getBid());
                            }
                        }
                        view.requestBidListByWishSuccess(bids);
                        break;
                    default:
                        view.requestBidForWishFailed("Erro ao buscar a lista de lances");
                }
            }

            @Override
            public void onFailure(Call<List<BidResponse>> call, Throwable t) {
                view.requestBidListByWishFailed(t.getMessage());
            }
        });
    }

    @Override
    public void requestAcceptBid(String wishId) {
        Call<BidResponse> request = AppRestManager.getInstance().getAppRestEndpoint().acceptBid(wishId);

        request.enqueue(new Callback<BidResponse>() {
            @Override
            public void onResponse(Call<BidResponse> call, Response<BidResponse> response) {
                switch (response.code()){
                    case 200:
                        view.requestAcceptBidSuccess(response.body().getBid());
                        break;
                    default:
                        view.requestAcceptBidFailed("Erro ao aceitar o lance");
                }
            }

            @Override
            public void onFailure(Call<BidResponse> call, Throwable t) {
                view.requestAcceptBidFailed(t.getMessage());
            }
        });

    }

    @Override
    public void requestBidWishWinner(String wishId) {
        Call<BidResponse> request = AppRestManager.getInstance().getAppRestEndpoint().getWishBidWinner(wishId);

        request.enqueue(new Callback<BidResponse>() {
            @Override
            public void onResponse(Call<BidResponse> call, Response<BidResponse> response) {
                switch (response.code()){
                    case 200:
                        view.requestBidWishWinnerSuccess(response.body().getBid());
                        break;
                    default:
                        view.requestBidWishWinnerFailed("Erro ao buscar o lance vencedor");
                }
            }

            @Override
            public void onFailure(Call<BidResponse> call, Throwable t) {
                view.requestBidWishWinnerFailed(t.getMessage());
            }
        });
    }
}
