package com.app.sample.shop.presenters;

import com.app.sample.shop.data.api.AppRestManager;
import com.app.sample.shop.data.api.responses.UserResponse;
import com.app.sample.shop.interfaces.LoginMVP;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements LoginMVP.Presenter {
    private LoginMVP.View view;

    public LoginPresenter(LoginMVP.View view) {
        this.view = view;
    }

    @Override
    public void requestLogin(String username, String password) {

        Call<UserResponse> call = AppRestManager.getInstance().getAppRestEndpoint().loginUser(username, password);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                switch (response.code()){
                    case 200:
                        view.requestLoginSuccess(response.body().getUser());
                        break;

                    default:
                        view.requestLoginFailed("Login inválido.");
                }

            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                view.requestLoginFailed(t.getMessage());
            }
        });
    }
}
